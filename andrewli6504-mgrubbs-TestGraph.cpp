// -------------
// TestGraph.cpp
// -------------

// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/index.html
// https://www.boost.org/doc/libs/1_76_0/libs/graph/doc/adjacency_list.html

// --------
// includes
// --------

#include <iostream> // cout, endl
#include <iterator> // ostream_iterator
#include <sstream>  // ostringstream
#include <utility>  // pair
#include <stack>    // stack?

#include "boost/graph/adjacency_list.hpp" // adjacency_list

#include "gtest/gtest.h"

#include "Graph.hpp"

// ------
// usings
// ------

using namespace std;
using namespace testing;

// -----
// Types
// -----

template <typename T>
struct GraphFixture : Test {
    // ------
    // usings
    // ------

    using graph_type          = T;
    using vertex_descriptor   = typename graph_type::vertex_descriptor;
    using edge_descriptor     = typename graph_type::edge_descriptor;
    using vertex_iterator     = typename graph_type::vertex_iterator;
    using edge_iterator       = typename graph_type::edge_iterator;
    using adjacency_iterator  = typename graph_type::adjacency_iterator;
    using vertices_size_type  = typename graph_type::vertices_size_type;
    using edges_size_type     = typename graph_type::edges_size_type;
};

/*
directed, sparse, unweighted
possibly connected
possibly cyclic
*/

/*
THINGS TO TEST:
add_edge
add_vertex
adjacent_vertices
edge
edges
num_edges
num_vertices
source
target
vertex
vertices
*/

using
graph_types =
    Types<
    boost::adjacency_list<boost::setS, boost::vecS, boost::directedS>,
    Graph
    >;

#ifdef __APPLE__
TYPED_TEST_CASE(GraphFixture, graph_types,);
#else
TYPED_TEST_CASE(GraphFixture, graph_types);
#endif

/**
 *  Tests 0 - 4 were given to us
 **/
TYPED_TEST(GraphFixture, test0) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    vertex_descriptor vd = vertex(0, g);
    ASSERT_EQ(vd, vdA);

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 1u);
}

TYPED_TEST(GraphFixture, test1) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;

    pair<edge_descriptor, bool> p1 = add_edge(vdA, vdB, g);
    ASSERT_EQ(p1.first,  edAB);
    ASSERT_EQ(p1.second, false);

    pair<edge_descriptor, bool> p2 = edge(vdA, vdB, g);
    ASSERT_EQ(p2.first,  edAB);
    ASSERT_EQ(p2.second, true);

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);

    vertex_descriptor vd1 = source(edAB, g);
    ASSERT_EQ(vd1, vdA);

    vertex_descriptor vd2 = target(edAB, g);
    ASSERT_EQ(vd2, vdB);
}

TYPED_TEST(GraphFixture, test2) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    pair<vertex_iterator, vertex_iterator> p = vertices(g);
    vertex_iterator                        b = p.first;
    vertex_iterator                        e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdA);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdB);
    ++b;
    ASSERT_EQ(b, e);
}

TYPED_TEST(GraphFixture, test3) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edge_iterator     = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;

    pair<edge_iterator, edge_iterator> p = edges(g);
    edge_iterator                      b = p.first;
    edge_iterator                      e = p.second;
    ASSERT_NE(b, e);

    edge_descriptor ed1 = *b;
    ASSERT_EQ(ed1, edAB);
    ++b;
    ASSERT_NE(b, e);

    edge_descriptor ed2 = *b;
    ASSERT_EQ(ed2, edAC);
    ++b;
    ASSERT_EQ(e, b);
}

TYPED_TEST(GraphFixture, test4) {
    using graph_type          = typename TestFixture::graph_type;
    using vertex_descriptor   = typename TestFixture::vertex_descriptor;
    using adjacency_iterator  = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);

    pair<adjacency_iterator, adjacency_iterator> p = adjacent_vertices(vdA, g);
    adjacency_iterator                           b = p.first;
    adjacency_iterator                           e = p.second;
    ASSERT_NE(b, e);

    vertex_descriptor vd1 = *b;
    ASSERT_EQ(vd1, vdB);
    ++b;
    ASSERT_NE(b, e);

    vertex_descriptor vd2 = *b;
    ASSERT_EQ(vd2, vdC);
    ++b;
    ASSERT_EQ(e, b);
}

//Tests getting vertices by index
TYPED_TEST(GraphFixture, test5) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertices_size_type = typename TestFixture::vertices_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    ASSERT_EQ(vdA, vertex(0, g));
    ASSERT_EQ(vdB, vertex(1, g));
    ASSERT_EQ(vdC, vertex(2, g));

    vertices_size_type vs = num_vertices(g);
    ASSERT_EQ(vs, 3u);
}

// Can't add same edge to graph
TYPED_TEST(GraphFixture, test6) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edges_size_type    = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g).first;

    for(int i = 0; i < 5; i++)
    {
        ASSERT_FALSE(add_edge(vdA, vdB, g).second);
    }

    edges_size_type es = num_edges(g);
    ASSERT_EQ(es, 1u);
}

// This test makes a complete graph and tests sources
TYPED_TEST(GraphFixture, test7) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);

    add_edge(vdA, vdB, g);
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    add_edge(vdA, vdC, g);
    add_edge(vdC, vdA, g);
    add_edge(vdA, vdD, g);
    add_edge(vdD, vdA, g);
    add_edge(vdA, vdE, g);
    add_edge(vdE, vdA, g);
    add_edge(vdA, vdF, g);
    add_edge(vdF, vdA, g);

    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    add_edge(vdC, vdB, g);
    edge_descriptor edBD = add_edge(vdB, vdD, g).first;
    add_edge(vdD, vdB, g);
    add_edge(vdB, vdE, g);
    add_edge(vdE, vdB, g);
    edge_descriptor edBF = add_edge(vdB, vdF, g).first;
    add_edge(vdF, vdB, g);

    add_edge(vdC, vdD, g);
    add_edge(vdD, vdC, g);
    add_edge(vdC, vdE, g);
    add_edge(vdE, vdC, g);
    add_edge(vdC, vdF, g);
    add_edge(vdF, vdC, g);

    add_edge(vdD, vdE, g);
    add_edge(vdE, vdD, g);
    add_edge(vdD, vdF, g);
    add_edge(vdF, vdD, g);

    add_edge(vdE, vdF, g);
    add_edge(vdF, vdE, g);

    edges_size_type k = num_edges(g);
    ASSERT_EQ(k, 30);
    vertex_descriptor sBC = source(edBC, g);
    vertex_descriptor sBA = source(edBA, g);
    vertex_descriptor sBD = source(edBD, g);
    vertex_descriptor sBF = source(edBF, g);
    ASSERT_EQ(sBC, sBA);
    ASSERT_EQ(sBC, sBD);
    ASSERT_EQ(sBC, sBF);
    ASSERT_EQ(sBA, sBD);
    ASSERT_EQ(sBA, sBF);
    ASSERT_EQ(sBD, sBF);
}

//Tests directed edges going both ways between two vertices work
TYPED_TEST(GraphFixture, test8) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor    = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;

    ASSERT_NE(edBA, edAB);
}

//Tests that target and source return the intended vertices
TYPED_TEST(GraphFixture, test9) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor    = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;

    ASSERT_EQ(source(edAB, g), vdA);
    ASSERT_NE(source(edAB, g), vdB);

    ASSERT_EQ(source(edBA, g), vdB);
    ASSERT_NE(source(edBA, g), vdA);

    ASSERT_EQ(target(edAB, g), vdB);
    ASSERT_NE(target(edAB, g), vdA);

    ASSERT_EQ(target(edBA, g), vdA);
    ASSERT_NE(target(edBA, g), vdB);
}

//Vertices iterators iterate properly and have nothing to do with edges
TYPED_TEST(GraphFixture, test10) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    add_edge(vdA, vdB, g).first;
    add_edge(vdB, vdA, g).first;
    add_edge(vdA, vdA, g).first;
    add_edge(vdB, vdB, g).first;
    add_edge(vdC, vdC, g).first;

    vertex_iterator vb = vertices(g).first;
    vertex_iterator ve = vertices(g).second;

    for(int i = 0; i < 3; i++)
    {
        ASSERT_NE(vb, ve);
        ++vb;
    }

    ASSERT_EQ(vb, ve);
}

//Edges iterator works properly and goes through every edge
TYPED_TEST(GraphFixture, test11) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_iterator    = typename TestFixture::edge_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdA, g);
    add_edge(vdB, vdB, g);

    edge_iterator eb = edges(g).first;
    edge_iterator ee = edges(g).second;

    for(int i = 0; i < 4; i++)
    {
        ASSERT_NE(eb, ee);
        ++eb;
    }

    ASSERT_EQ(eb, ee);
}

//Adjacency iterator on vertex with no edges
TYPED_TEST(GraphFixture, test12) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using adjacency_iterator   = typename TestFixture::adjacency_iterator;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);

    adjacency_iterator vb = adjacent_vertices(vdC, g).first;
    adjacency_iterator ve = adjacent_vertices(vdC, g).second;

    ASSERT_EQ(vb, ve);
}

/**
 * This is once again a complete graph and this time tests
 * num_edges, num_vertices, and whether the iterator goes through
 * all edges and vertices.
**/

TYPED_TEST(GraphFixture, test13) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edges_size_type   = typename TestFixture::edges_size_type;
    using edge_iterator   = typename TestFixture::edge_iterator;
    using vertex_iterator   = typename TestFixture::vertex_iterator;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdC, g);
    add_edge(vdC, vdA, g);
    add_edge(vdA, vdD, g);
    add_edge(vdD, vdA, g);
    add_edge(vdA, vdE, g);
    add_edge(vdE, vdA, g);

    add_edge(vdB, vdC, g);
    add_edge(vdC, vdB, g);
    add_edge(vdB, vdD, g);
    add_edge(vdD, vdB, g);
    add_edge(vdB, vdE, g);
    add_edge(vdE, vdB, g);

    add_edge(vdC, vdD, g);
    add_edge(vdD, vdC, g);
    add_edge(vdC, vdE, g);
    add_edge(vdE, vdC, g);

    add_edge(vdD, vdE, g);
    add_edge(vdE, vdD, g);

    edges_size_type k = num_edges(g);
    size_t counter = 0;
    edge_iterator b = edges(g).first;
    edge_iterator e = edges(g).second;
    while (b != e) {
        ++counter;
        ++b;
    }
    vertex_iterator b1 = vertices(g).first;
    vertex_iterator e1 = vertices(g).second;
    size_t counter1 = 0;
    while (b1 != e1) {
        ++counter1;
        ++b1;
    }
    ASSERT_EQ(k, counter);
    ASSERT_EQ(num_vertices(g), counter1);

}

//Adjacency iterator on a small complete graph
TYPED_TEST(GraphFixture, test14) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using adjacency_iterator   = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdA, g);
    add_edge(vdB, vdB, g);

    adjacency_iterator vb = adjacent_vertices(vdA, g).first;
    adjacency_iterator ve = adjacent_vertices(vdA, g).second;

    ASSERT_EQ(*vb, vdA);
    ++vb;
    ASSERT_EQ(*vb, vdB);
    ++vb;

    ASSERT_EQ(vb, ve);
}

//Tests that add vertex adds the next index properly
TYPED_TEST(GraphFixture, test15) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;

    ASSERT_NE(add_vertex(g), add_vertex(g));
    ASSERT_EQ(vertex(2, g), add_vertex(g));
}

//Edge that starts and ends at the same vertex
TYPED_TEST(GraphFixture, test16) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using edge_descriptor    = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    edge_descriptor edAA = add_edge(vdA, vdA, g).first;

    ASSERT_EQ(source(edAA, g), target(edAA, g));
}

/**
 * This is a complete graph with a lot of tests for source and target.
 * Edges are compared to have the same target.
 **/
TYPED_TEST(GraphFixture, test17) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    edge_descriptor edAD = add_edge(vdA, vdD, g).first;
    edge_descriptor edDA = add_edge(vdD, vdA, g).first;
    edge_descriptor edAE = add_edge(vdA, vdE, g).first;
    edge_descriptor edEA = add_edge(vdE, vdA, g).first;

    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edCB = add_edge(vdC, vdB, g).first;
    edge_descriptor edBD = add_edge(vdB, vdD, g).first;
    edge_descriptor edDB = add_edge(vdD, vdB, g).first;
    edge_descriptor edBE = add_edge(vdB, vdE, g).first;
    edge_descriptor edEB = add_edge(vdE, vdB, g).first;

    edge_descriptor edCD = add_edge(vdC, vdD, g).first;
    edge_descriptor edDC = add_edge(vdD, vdC, g).first;
    edge_descriptor edCE = add_edge(vdC, vdE, g).first;
    edge_descriptor edEC = add_edge(vdE, vdC, g).first;

    edge_descriptor edDE = add_edge(vdD, vdE, g).first;
    edge_descriptor edED = add_edge(vdE, vdD, g).first;

    ASSERT_EQ(target(edCD, g), target(edBD, g));
    ASSERT_EQ(target(edBC, g), target(edAC, g));
    ASSERT_EQ(target(edEC, g), target(edDC, g));
    ASSERT_EQ(target(edAE, g), target(edCE, g));
    ASSERT_EQ(target(edAD, g), target(edED, g));
    ASSERT_EQ(target(edCA, g), target(edEA, g));
    ASSERT_EQ(target(edEB, g), target(edAB, g));
    ASSERT_EQ(target(edDE, g), target(edBE, g));
    ASSERT_EQ(source(edCD, g), target(edDC, g));
    ASSERT_EQ(source(edDC, g), target(edCD, g));
    ASSERT_EQ(source(edEB, g), target(edBE, g));
    ASSERT_EQ(source(edDB, g), target(edED, g));
    ASSERT_EQ(source(edBA, g), target(edAB, g));
    ASSERT_EQ(source(edDA, g), target(edAD, g));
    ASSERT_EQ(source(edCB, g), target(edBC, g));
}

//Creates a cycle, and then iterates through the entire cycle until it reaches where it started
TYPED_TEST(GraphFixture, test18) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;
    using adjacency_iterator   = typename TestFixture::adjacency_iterator;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    vertex_iterator b1 = vertices(g).first;
    vertex_iterator e1 = vertices(g).second;
    while(b1 + 1 != e1)
    {
        vertex_descriptor vdA = *b1;
        ++b1;
        vertex_descriptor vdB = *b1;
        add_edge(vdA, vdB, g);
    }

    vertex_descriptor start = vertex(0, g);
    vertex_descriptor v = vertex(0, g);

    for(int i = 0; i < 5; i++)
    {
        adjacency_iterator b2 = adjacent_vertices(v, g).first;
        v = *b2;
    }

    ASSERT_EQ(start, v);
}

/**
 * Creates a complete graph and then checks that all the vertices
 * have the same amount of edges.
 **/
TYPED_TEST(GraphFixture, test19) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator   = typename TestFixture::vertex_iterator;
    using adjacency_iterator   = typename TestFixture::adjacency_iterator;


    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdC, g);
    add_edge(vdC, vdA, g);
    add_edge(vdA, vdD, g);
    add_edge(vdD, vdA, g);

    add_edge(vdB, vdC, g);
    add_edge(vdC, vdB, g);
    add_edge(vdB, vdD, g);
    add_edge(vdD, vdB, g);

    add_edge(vdC, vdD, g);
    add_edge(vdD, vdC, g);


    size_t adjA = 0;
    size_t adjB = 0;
    size_t adjC = 0;
    size_t adjD = 0;
    vertex_iterator b = vertices(g).first;
    vertex_iterator e = vertices(g).second;
    while (b != e) {
        vertex_descriptor v = *b;
        adjacency_iterator ab = adjacent_vertices(v, g).first;
        adjacency_iterator ae = adjacent_vertices(v, g).second;
        while (ab != ae) {
            if (v == vdA) {
                ++adjA;
            }
            if (v == vdB) {
                ++adjB;
            }
            if (v == vdC) {
                ++adjC;
            }
            if (v == vdD) {
                ++adjD;
            }
            ++ab;
        }
        ++b;
    }
    ASSERT_EQ(adjA, adjB);
    ASSERT_EQ(adjA, adjC);
    ASSERT_EQ(adjA, adjD);
    ASSERT_EQ(adjB, adjC);
    ASSERT_EQ(adjB, adjD);
    ASSERT_EQ(adjC, adjD);
}

TYPED_TEST(GraphFixture, test20) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);

    ASSERT_EQ(vdA, vertex(0, g));
    ASSERT_FALSE(edge(vdA, vdA, g).second);
}

/**
 * Creates complete graph and checks that the edges are placed
 **/
TYPED_TEST(GraphFixture, test21) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdC, g);
    add_edge(vdC, vdA, g);
    add_edge(vdA, vdD, g);
    add_edge(vdD, vdA, g);
    add_edge(vdA, vdE, g);
    add_edge(vdE, vdA, g);
    add_edge(vdA, vdF, g);
    add_edge(vdF, vdA, g);

    add_edge(vdB, vdC, g);
    add_edge(vdC, vdB, g);
    add_edge(vdB, vdD, g);
    add_edge(vdD, vdB, g);
    add_edge(vdB, vdE, g);
    add_edge(vdE, vdB, g);
    add_edge(vdB, vdF, g);
    add_edge(vdF, vdB, g);

    add_edge(vdC, vdD, g);
    add_edge(vdD, vdC, g);
    add_edge(vdC, vdE, g);
    add_edge(vdE, vdC, g);
    add_edge(vdC, vdF, g);
    add_edge(vdF, vdC, g);

    add_edge(vdD, vdE, g);
    add_edge(vdE, vdD, g);
    add_edge(vdD, vdF, g);
    add_edge(vdF, vdD, g);

    add_edge(vdE, vdF, g);
    add_edge(vdF, vdE, g);

    vertex_iterator b = vertices(g).first;
    vertex_iterator e = vertices(g).second;

    for(; b != e; ++b) {
        vertex_iterator b2 = vertices(g).first;
        vertex_iterator e2 = vertices(g).second;
        for(; b2 != e2; ++b2) {
            if (*b != *b2) {
                ASSERT_TRUE(edge(*b, *b2, g).second);
            }
        }
    }
}

/**
 * Creates complete graph and checks that the vertices are not connected to themselves
 **/
TYPED_TEST(GraphFixture, test22) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdC, g);
    add_edge(vdC, vdA, g);
    add_edge(vdA, vdD, g);
    add_edge(vdD, vdA, g);
    add_edge(vdA, vdE, g);
    add_edge(vdE, vdA, g);
    add_edge(vdA, vdF, g);
    add_edge(vdF, vdA, g);

    add_edge(vdB, vdC, g);
    add_edge(vdC, vdB, g);
    add_edge(vdB, vdD, g);
    add_edge(vdD, vdB, g);
    add_edge(vdB, vdE, g);
    add_edge(vdE, vdB, g);
    add_edge(vdB, vdF, g);
    add_edge(vdF, vdB, g);

    add_edge(vdC, vdD, g);
    add_edge(vdD, vdC, g);
    add_edge(vdC, vdE, g);
    add_edge(vdE, vdC, g);
    add_edge(vdC, vdF, g);
    add_edge(vdF, vdC, g);

    add_edge(vdD, vdE, g);
    add_edge(vdE, vdD, g);
    add_edge(vdD, vdF, g);
    add_edge(vdF, vdD, g);

    add_edge(vdE, vdF, g);
    add_edge(vdF, vdE, g);

    vertex_iterator b = vertices(g).first;
    vertex_iterator e = vertices(g).second;

    for(; b != e; ++b) {
        vertex_iterator b2 = vertices(g).first;
        vertex_iterator e2 = vertices(g).second;
        for(; b2 != e2; ++b2) {
            if (*b == *b2) {
                ASSERT_FALSE(edge(*b, *b2, g).second);
            }
        }
    }
}

/**
 * Just makes vertices. Checks that there are no edges.s
 **/
TYPED_TEST(GraphFixture, test23) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_iterator     = typename TestFixture::vertex_iterator;

    graph_type g;

    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);
    add_vertex(g);

    vertex_iterator b = vertices(g).first;
    vertex_iterator e = vertices(g).second;

    for(; b != e; ++b) {
        vertex_iterator b2 = vertices(g).first;
        vertex_iterator e2 = vertices(g).second;
        for(; b2 != e2; ++b2) {
            ASSERT_FALSE(edge(*b, *b2, g).second);
        }
    }
}

/**
 * Creates complete graph and checks that vertices are in order
 * uses vertex and source to check
 **/
TYPED_TEST(GraphFixture, test24) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;
    using edge_descriptor   = typename TestFixture::edge_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);

    edge_descriptor edAB = add_edge(vdA, vdB, g).first;
    edge_descriptor edBA = add_edge(vdB, vdA, g).first;
    edge_descriptor edAC = add_edge(vdA, vdC, g).first;
    edge_descriptor edCA = add_edge(vdC, vdA, g).first;
    edge_descriptor edAD = add_edge(vdA, vdD, g).first;
    edge_descriptor edDA = add_edge(vdD, vdA, g).first;
    edge_descriptor edAE = add_edge(vdA, vdE, g).first;
    edge_descriptor edEA = add_edge(vdE, vdA, g).first;
    edge_descriptor edAF = add_edge(vdA, vdF, g).first;
    edge_descriptor edFA = add_edge(vdF, vdA, g).first;

    edge_descriptor edBC = add_edge(vdB, vdC, g).first;
    edge_descriptor edCB = add_edge(vdC, vdB, g).first;
    edge_descriptor edBD = add_edge(vdB, vdD, g).first;
    edge_descriptor edDB = add_edge(vdD, vdB, g).first;
    edge_descriptor edBE = add_edge(vdB, vdE, g).first;
    edge_descriptor edEB = add_edge(vdE, vdB, g).first;
    edge_descriptor edBF = add_edge(vdB, vdF, g).first;
    edge_descriptor edFB = add_edge(vdF, vdB, g).first;

    edge_descriptor edCD = add_edge(vdC, vdD, g).first;
    edge_descriptor edDC = add_edge(vdD, vdC, g).first;
    edge_descriptor edCE = add_edge(vdC, vdE, g).first;
    edge_descriptor edEC = add_edge(vdE, vdC, g).first;
    edge_descriptor edCF = add_edge(vdC, vdF, g).first;
    edge_descriptor edFC = add_edge(vdF, vdC, g).first;

    edge_descriptor edDE = add_edge(vdD, vdE, g).first;
    edge_descriptor edED = add_edge(vdE, vdD, g).first;
    edge_descriptor edDF = add_edge(vdD, vdF, g).first;
    edge_descriptor edFD = add_edge(vdF, vdD, g).first;

    edge_descriptor edEF = add_edge(vdE, vdF, g).first;
    edge_descriptor edFE = add_edge(vdF, vdE, g).first;

    ASSERT_EQ(vertex(0, g), source(edAB, g));
    ASSERT_EQ(vertex(0, g), source(edAC, g));
    ASSERT_EQ(vertex(0, g), source(edAD, g));
    ASSERT_EQ(vertex(0, g), source(edAE, g));
    ASSERT_EQ(vertex(0, g), source(edAF, g));
    ASSERT_EQ(vertex(1, g), source(edBA, g));
    ASSERT_EQ(vertex(1, g), source(edBC, g));
    ASSERT_EQ(vertex(1, g), source(edBD, g));
    ASSERT_EQ(vertex(1, g), source(edBE, g));
    ASSERT_EQ(vertex(1, g), source(edBF, g));
    ASSERT_EQ(vertex(2, g), source(edCB, g));
    ASSERT_EQ(vertex(2, g), source(edCD, g));
    ASSERT_EQ(vertex(2, g), source(edCE, g));
    ASSERT_EQ(vertex(2, g), source(edCF, g));
    ASSERT_EQ(vertex(2, g), source(edCA, g));
    ASSERT_EQ(vertex(3, g), source(edDA, g));
    ASSERT_EQ(vertex(3, g), source(edDB, g));
    ASSERT_EQ(vertex(3, g), source(edDC, g));
    ASSERT_EQ(vertex(3, g), source(edDE, g));
    ASSERT_EQ(vertex(3, g), source(edDF, g));
    ASSERT_EQ(vertex(4, g), source(edEA, g));
    ASSERT_EQ(vertex(4, g), source(edEB, g));
    ASSERT_EQ(vertex(4, g), source(edEC, g));
    ASSERT_EQ(vertex(4, g), source(edED, g));
    ASSERT_EQ(vertex(4, g), source(edEF, g));
    ASSERT_EQ(vertex(5, g), source(edFA, g));
    ASSERT_EQ(vertex(5, g), source(edFB, g));
    ASSERT_EQ(vertex(5, g), source(edFC, g));
    ASSERT_EQ(vertex(5, g), source(edFD, g));
    ASSERT_EQ(vertex(5, g), source(edFE, g));
}

/**
 * Creates cyclical graph and checks that each edge was placed
 **/
TYPED_TEST(GraphFixture, test25) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdD, g);
    add_edge(vdD, vdE, g);
    add_edge(vdE, vdF, g);
    add_edge(vdF, vdA, g);

    size_t index = 0;
    while (index < num_vertices(g) - 1) {
        ASSERT_TRUE(edge(vertex(index, g), vertex(index + 1, g), g).second);
        ++index;
    }
}

/**
 * Creates a graph mapping vertex i to vertex i / 2
 * checks that that is the case with edge and vertex
 **/
TYPED_TEST(GraphFixture, test26) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);

    add_edge(vdA, vdA, g);
    add_edge(vdB, vdA, g);
    add_edge(vdC, vdB, g);
    add_edge(vdD, vdB, g);
    add_edge(vdE, vdC, g);
    add_edge(vdF, vdC, g);

    size_t index = 0;
    while (index < num_vertices(g)) {
        ASSERT_TRUE(edge(vertex(index, g), vertex(index / 2, g), g).second);
        ++index;
    }
}

/**
 * Creates complete graph and checks that it is impossible to add any edges
 * in the way that test cases 25 and 26 were added.
 **/
TYPED_TEST(GraphFixture, test27) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdC, g);
    add_edge(vdC, vdA, g);
    add_edge(vdA, vdD, g);
    add_edge(vdD, vdA, g);
    add_edge(vdA, vdE, g);
    add_edge(vdE, vdA, g);
    add_edge(vdA, vdF, g);
    add_edge(vdF, vdA, g);

    add_edge(vdB, vdC, g);
    add_edge(vdC, vdB, g);
    add_edge(vdB, vdD, g);
    add_edge(vdD, vdB, g);
    add_edge(vdB, vdE, g);
    add_edge(vdE, vdB, g);
    add_edge(vdB, vdF, g);
    add_edge(vdF, vdB, g);

    add_edge(vdC, vdD, g);
    add_edge(vdD, vdC, g);
    add_edge(vdC, vdE, g);
    add_edge(vdE, vdC, g);
    add_edge(vdC, vdF, g);
    add_edge(vdF, vdC, g);

    add_edge(vdD, vdE, g);
    add_edge(vdE, vdD, g);
    add_edge(vdD, vdF, g);
    add_edge(vdF, vdD, g);

    add_edge(vdE, vdF, g);
    add_edge(vdF, vdE, g);

    for (size_t i = 0; i < num_vertices(g) - 1; ++i) {
        ASSERT_FALSE(add_edge(vertex(i, g), vertex(i + 1, g), g).second);
        if (vertex(i, g) != vertex(i / 2, g)) {
            ASSERT_FALSE(add_edge(vertex(i, g), vertex(i / 2, g), g).second);
        }
    }
}

/**
 * Creates a complete graph and then adds a vertex and connects that vertex
 * to every other vertex in both directions to complete the graph again.
 **/
TYPED_TEST(GraphFixture, test28) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdC, g);
    add_edge(vdC, vdA, g);
    add_edge(vdA, vdD, g);
    add_edge(vdD, vdA, g);
    add_edge(vdA, vdE, g);
    add_edge(vdE, vdA, g);
    add_edge(vdA, vdF, g);
    add_edge(vdF, vdA, g);

    add_edge(vdB, vdC, g);
    add_edge(vdC, vdB, g);
    add_edge(vdB, vdD, g);
    add_edge(vdD, vdB, g);
    add_edge(vdB, vdE, g);
    add_edge(vdE, vdB, g);
    add_edge(vdB, vdF, g);
    add_edge(vdF, vdB, g);

    add_edge(vdC, vdD, g);
    add_edge(vdD, vdC, g);
    add_edge(vdC, vdE, g);
    add_edge(vdE, vdC, g);
    add_edge(vdC, vdF, g);
    add_edge(vdF, vdC, g);

    add_edge(vdD, vdE, g);
    add_edge(vdE, vdD, g);
    add_edge(vdD, vdF, g);
    add_edge(vdF, vdD, g);

    add_edge(vdE, vdF, g);
    add_edge(vdF, vdE, g);

    vertex_descriptor vdG = add_vertex(g);
    for (size_t i = 0; i < num_vertices(g) - 1; ++i) {
        ASSERT_TRUE(add_edge(vdG, vertex(i, g), g).second);
        ASSERT_TRUE(add_edge(vertex(i, g), vdG, g).second);
    }
}

/**
 * Creates a complete graph and checks that vertices are in order and edges
 * exist between each other with edge and vertex
 **/
TYPED_TEST(GraphFixture, test29) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdC, g);
    add_edge(vdC, vdA, g);
    add_edge(vdA, vdD, g);
    add_edge(vdD, vdA, g);
    add_edge(vdA, vdE, g);
    add_edge(vdE, vdA, g);
    add_edge(vdA, vdF, g);
    add_edge(vdF, vdA, g);

    add_edge(vdB, vdC, g);
    add_edge(vdC, vdB, g);
    add_edge(vdB, vdD, g);
    add_edge(vdD, vdB, g);
    add_edge(vdB, vdE, g);
    add_edge(vdE, vdB, g);
    add_edge(vdB, vdF, g);
    add_edge(vdF, vdB, g);

    add_edge(vdC, vdD, g);
    add_edge(vdD, vdC, g);
    add_edge(vdC, vdE, g);
    add_edge(vdE, vdC, g);
    add_edge(vdC, vdF, g);
    add_edge(vdF, vdC, g);

    add_edge(vdD, vdE, g);
    add_edge(vdE, vdD, g);
    add_edge(vdD, vdF, g);
    add_edge(vdF, vdD, g);

    add_edge(vdE, vdF, g);
    add_edge(vdF, vdE, g);

    size_t i = 0;
    while (i < num_edges(g)) {
        size_t index = i % num_vertices(g);
        size_t index2 = index - 1 < num_vertices(g) ? index - 1 : num_vertices(g) - 1;
        ASSERT_TRUE(edge(vertex(index, g), vertex(index2, g), g).second);
        ASSERT_FALSE(add_edge(vertex(index, g), vertex(index2, g), g).second);
        ++i;
    }
}

/**
 * Creates complete graph and checks that any two indices of the vertices are connected
 * other than a vertex with itself. Does this randomly 1000 times to ensure that edge
 * function do not change the graph
 **/
TYPED_TEST(GraphFixture, test30) {
    using graph_type        = typename TestFixture::graph_type;
    using vertex_descriptor = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdA, g);
    add_edge(vdA, vdC, g);
    add_edge(vdC, vdA, g);
    add_edge(vdA, vdD, g);
    add_edge(vdD, vdA, g);
    add_edge(vdA, vdE, g);
    add_edge(vdE, vdA, g);
    add_edge(vdA, vdF, g);
    add_edge(vdF, vdA, g);

    add_edge(vdB, vdC, g);
    add_edge(vdC, vdB, g);
    add_edge(vdB, vdD, g);
    add_edge(vdD, vdB, g);
    add_edge(vdB, vdE, g);
    add_edge(vdE, vdB, g);
    add_edge(vdB, vdF, g);
    add_edge(vdF, vdB, g);

    add_edge(vdC, vdD, g);
    add_edge(vdD, vdC, g);
    add_edge(vdC, vdE, g);
    add_edge(vdE, vdC, g);
    add_edge(vdC, vdF, g);
    add_edge(vdF, vdC, g);

    add_edge(vdD, vdE, g);
    add_edge(vdE, vdD, g);
    add_edge(vdD, vdF, g);
    add_edge(vdF, vdD, g);

    add_edge(vdE, vdF, g);
    add_edge(vdF, vdE, g);

    for (int i = 0; i < 1000; ++i) {
        size_t rand_index = rand() % num_vertices(g);
        size_t rand_index2 = rand() % num_vertices(g);
        if (rand_index != rand_index2) {
            ASSERT_TRUE(edge(vertex(rand_index, g), vertex(rand_index2, g), g).second);
            ASSERT_FALSE(add_edge(vertex(rand_index, g), vertex(rand_index2, g), g).second);
        } else {
            ASSERT_FALSE(edge(vertex(rand_index, g), vertex(rand_index2, g), g).second);
        }
    }
}

/**
 * Constructs graph with 1000 vertices and connects vertex i to vertex i + 1
 * This results in a graph with 999 edges that is not cyclical
 **/
TYPED_TEST(GraphFixture, test31) {
    using graph_type        = typename TestFixture::graph_type;

    graph_type g;
    for (size_t i = 0; i < 1000; ++i) {
        add_vertex(g);
    }
    for (size_t i = 0; i < 999; ++i) {
        ASSERT_TRUE(add_edge(vertex(i, g), vertex(i + 1, g), g).second);
    }
    ASSERT_EQ(num_edges(g), 999);
}

/**
 * LONG TEST - Makes a graph with 1000 vertices and then connects them all.
 * Results in a directed complete graph which should have n * (n - 1) edges
 * where n is the amount of vertices
 * Depending on valid method, could become COLOSSAL TEST
 **/
TYPED_TEST(GraphFixture, test32) {
    using graph_type        = typename TestFixture::graph_type;

    graph_type g;
    for (size_t i = 0; i < 1000; ++i) {
        add_vertex(g);
    }
    for (size_t i = 0; i < 1000; ++i) {
        for (size_t i2 = 0; i2 < 1000; ++i2) {
            if (i != i2) {
                ASSERT_TRUE(add_edge(vertex(i, g), vertex(i2, g), g).second);
            }
        }
    }
    ASSERT_EQ(num_edges(g), 1000 * 999);
}

/**
 * Creates a tree and does a depth first search on that tree.
 * Relies on adjacency_iterator
 * (DFS is a bit backwards but is still DFS)
 **/
TYPED_TEST(GraphFixture, test33) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);
    vertex_descriptor vdF = add_vertex(g);
    vertex_descriptor vdG = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdA, vdC, g);
    add_edge(vdB, vdD, g);
    add_edge(vdB, vdE, g);
    add_edge(vdC, vdF, g);
    add_edge(vdC, vdG, g);

    stack<vertex_descriptor> fringe;
    fringe.push(vdA);
    vertex_descriptor goal = vertex(6, g);
    bool success = false;
    size_t steps = 0;
    while (!fringe.empty() && !success) {
        vertex_descriptor curr = fringe.top();
        fringe.pop();
        if (curr == goal) {
            success = true;
        } else {
            adjacency_iterator b = adjacent_vertices(curr, g).first;
            adjacency_iterator e = adjacent_vertices(curr, g).second;
            while (b != e) {
                if (*b != curr) {
                    fringe.push(*b);
                }
                ++b;
            }
        }
        ++steps;
    }
    ASSERT_TRUE(success);
    ASSERT_EQ(steps, 3);
}

//Adjacency list with edges going both ways
TYPED_TEST(GraphFixture, test34) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    add_vertex(g);

    for(int i = 0; i < 4; i++)
    {
        add_edge(vertex(i, g), vertex(i + 1, g), g);
        add_edge(vertex(i + 1, g), vertex(i, g), g);
    }

    ASSERT_EQ(num_edges(g), 8u);

    adjacency_iterator b = adjacent_vertices(vdC, g).first;
    adjacency_iterator e = adjacent_vertices(vdC, g).second;

    ASSERT_EQ(*b, vdB);
    ++b;
    ASSERT_EQ(*b, vdD);
    ++b;
    ASSERT_EQ(b, e);
}

//Checks for directionality
TYPED_TEST(GraphFixture, test35) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);

    add_edge(vdA, vdB, g).first;

    ASSERT_TRUE(edge(vdA, vdB, g).second);
    ASSERT_FALSE(edge(vdB, vdA, g).second);
}

//Iterator access earlier indexes first
TYPED_TEST(GraphFixture, test36) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;
    using adjacency_iterator = typename TestFixture::adjacency_iterator;

    graph_type g;

    vertex_descriptor vdA = add_vertex(g);
    vertex_descriptor vdB = add_vertex(g);
    vertex_descriptor vdC = add_vertex(g);
    vertex_descriptor vdD = add_vertex(g);
    vertex_descriptor vdE = add_vertex(g);

    add_edge(vdA, vdB, g);
    add_edge(vdB, vdC, g);
    add_edge(vdC, vdD, g);
    add_edge(vdD, vdE, g);
    add_edge(vdA, vdE, g);

    vertex_descriptor v = vdA;

    int counter = 0;

    while(v != vdE)
    {
        adjacency_iterator b = adjacent_vertices(v, g).first;
        v = *b;
        ++counter;
    }

    ASSERT_EQ(v, vdE);
    ASSERT_EQ(counter, 4);
}

//Creating a complete graph, and checking that the edges and vertices are updated accordingly as we go
TYPED_TEST(GraphFixture, test37) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g;

    unsigned counter = 0;

    for(unsigned i = 1u; i <= 10u; i++)
    {
        vertex_descriptor v = add_vertex(g);
        for(unsigned j = 0u; j < num_vertices(g); j++)
        {
            add_edge(v, vertex(j, g), g);
        }

        ASSERT_EQ(num_vertices(g), i);
        ASSERT_EQ(num_edges(g), counter += num_vertices(g));
    }
}

//Testing that add_edge will create vertices if they don't exist yet
TYPED_TEST(GraphFixture, test38) {
    using graph_type         = typename TestFixture::graph_type;

    graph_type g;

    ASSERT_TRUE(add_edge(vertex(0, g), vertex(1, g), g).second);
    ASSERT_EQ(num_vertices(g), 2);
    ASSERT_EQ(num_edges(g), 1);
}

//Testing using multiple graphs with each other
TYPED_TEST(GraphFixture, test39) {
    using graph_type         = typename TestFixture::graph_type;
    using vertex_descriptor  = typename TestFixture::vertex_descriptor;

    graph_type g1;
    graph_type g2;

    vertex_descriptor vdA1 = add_vertex(g1);
    vertex_descriptor vdA2 = add_vertex(g2);

    ASSERT_EQ(vdA1, vdA2);

    add_edge(vdA1, vdA2, g1).first;
    add_edge(vdA1, vdA2, g2).first;

    ASSERT_EQ(num_vertices(g1), 1u);
    ASSERT_EQ(num_vertices(g2), 1u);
    ASSERT_EQ(num_edges(g1), 1u);
    ASSERT_EQ(num_edges(g2), 1u);

    ASSERT_TRUE(edge(vdA1, vdA2, g1).second);
    ASSERT_TRUE(edge(vdA2, vdA1, g1).second);
    ASSERT_TRUE(edge(vdA1, vdA1, g1).second);
    ASSERT_TRUE(edge(vdA2, vdA2, g1).second);

    ASSERT_TRUE(edge(vdA1, vdA2, g2).second);
    ASSERT_TRUE(edge(vdA2, vdA1, g2).second);
    ASSERT_TRUE(edge(vdA1, vdA1, g2).second);
    ASSERT_TRUE(edge(vdA2, vdA2, g2).second);
}